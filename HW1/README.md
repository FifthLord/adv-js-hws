# advanced-js-homework1-es6-classes

# 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Прототипне наслідування - це наслідування від батьківського об'єкту за принципом копіювання його прототипу, в прототипі ж містяться властивості та методи об'єктів.
Метод для прототипного наслідування - let childObj = Object.create(parentObj);

# 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
Для ініціалізації властивостей та методів батьківського класу. 
