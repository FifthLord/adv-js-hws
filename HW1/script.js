

class Employee {
   constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
   }

   get name() {
      return this._name
   }
   get age() {
      return this._age
   }
   get salary() {
      return this._salary
   }

   set name(name) {
      this._name = name
   }
   set age(age) {
      this._age = age
   }
   set salary(salary) {
      this._salary = salary
   }
};

class Programmer extends Employee {
   constructor(name, age, salary, lang) {
      super(name, age, salary)
      this._lang = lang
   }

   get salary() {
      return (this._salary) * 3
   }
   get lang() {
      return this._lang;
   }

   set lang(lang) {
      this._lang = lang;
   }
};


let programmer1 = new Programmer("Mykola oarsman", 26, 500, ['JS']);
let programmer2 = new Programmer("Slave Driver", 43, 2000, ['JS', 'Python', 'Java']);
let programmer3 = new Programmer("Galley Master", 113, 10000000, ['JS', 'Python', 'Java', 'Reptiloid']);
console.log(programmer1, programmer1.salary);
console.log(programmer2);
console.log(programmer3);